# Constanst for the object files.
# ALLFILES could be only MAINFILES but not sure of the syntax to concatenate MAINFILES and LIBS?
ALLFILES = main/main.o main/getInput.o lib/libcomponent.o lib/libpower.o lib/libresistance.o
LIBS = lib/libcomponent.o lib/libpower.o lib/libresistance.o

all : $(ALLFILES) 
	cc $(ALLFILES) -o ./electrotest

lib : $(LIBS)
	cc $(LIBS)
# todo: link electrotest to local libraries, in lib/

main/main.o : main/main.c main/getInput.c main/getInput.h
	cc -c main/main.c
lib/libcomponent.o : lib/libcomponent.c lib/libcomponent.h 
	cc -c lib/libcomponent.c
lib/libpower.o : lib/libpower.c lib/libpower.h
	cc -c 
lib/libresistance.o : lib/libresistance.c lib/libresistance.h
	cc -c lib/libresistance.c

.PHONY : install
install :
	cp electrotest /usr/bin/
# todo: link electrotest to installed libraries in /usr/lib
